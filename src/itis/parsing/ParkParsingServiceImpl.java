package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.Scanner;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        Park park = null;
        String title = parsingTitle();
        String ownerInn = parsingOwnerInn();
        LocalDate year = parsingFoundationYear();

        //write your code here
        try {
            Class parkClass = Class.forName (Park.class.getName());
            Class[] params = {String.class, String.class, LocalDate.class};
            Field[] declaredFields = parkClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (declaredField.isAnnotationPresent(FieldName.class)) {
                     declaredField.set(park,parsingTitle());
                }
                if (declaredField.isAnnotationPresent(MaxLength.class)) {
                    declaredField.set(park, parsingOwnerInn());
                }
                if (declaredField.isAnnotationPresent(NotBlank.class)) {
                    declaredField.set(park, parsingFoundationYear());
                }
                park = (Park) parkClass.getConstructor(params).newInstance(title,ownerInn,year);
            }
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return park;
    }


    public static String parsingTitle() {
        String legalName = null;
        try {
            FileReader fileReader = new FileReader("resources/parkdata.txt");
            Scanner scanner = new Scanner(fileReader);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(":");
                if (parts[0].equals("title")) {
                    legalName = parts[1];
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return legalName;
    }

    public static String parsingOwnerInn() {
        String ownerInn = null;
        try {
            FileReader fileReader = new FileReader("resources/parkdata.txt");
            Scanner scanner = new Scanner(fileReader);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(":");
                if (parts[0].equals("ownerOrganizationInn") && parts[1].length() == 13) {
                    ownerInn = parts[1];
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ownerInn;
    }

    public static LocalDate parsingFoundationYear() {
        LocalDate year = null;
        try {
            FileReader fileReader = new FileReader("resources/parkdata.txt");
            Scanner scanner = new Scanner(fileReader);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(":");
                if (parts[0].equals("foundationYear") && parts[1] != null && !parts[1].equals(" ")) {
                    year = LocalDate.parse(parts[1]);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return year;
    }
}
